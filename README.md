# Procject_task_1

IpDateApi API returns actual date time and ip host. 🚀"
Microservice using FastApi, which is conterized with Docker.

## Design Assumptions
* Endpoint GET ("/"), returning data in format json
```json
{
    'ip': "127.0.0.1', // ip host 
    'timestamp': ”2022.03.07 12:00” // datetime now
}
```
* Unit testing microservice
* Conterizing whit Docker, used `docker-compose.yml` in port 8080
* Creating a GitLab pipeline to build a docker image


## Technological Stack

File `requirements.txt` contains more information.

## Getting Started

#### Install
* Create virtualenv
* Clone repository, with GitLab
* Install tech stack, with file `requirements.txt`
  ```cmd
  pip install -r requirements.txt
  ```

#### Write app FastApi
* create folder `app` and `test`
* create app   `app/main.py`
* Start app 
```cmd
   uvicorn app.main:app --host 0.0.0.0 --port 8080
```
* Manual test app `http://0.0.0.0:8080/docs`
* Testing unit tests app
* Create file `Dockerfile` , next create file `docker-compose.yml`
* 
* Testing conterizing image, with docker
  used file Dockerfile
  ```cmd
    docker image rm -f myimage
    docker container rm -f mycontainer
  ```
  used file docker-compose
  ```cmd
  docker compose up --build
  ```
 * Manual test app `http://0.0.0.0:8080/docs`


## Status
* Success in completing each task

