
from datetime import datetime
from pickle import TRUE
from app.main import get_app
import pytest
import ipaddress
from httpx import AsyncClient


@pytest.mark.anyio
async def testGetIpDate():
    async with AsyncClient(app=get_app(), base_url="http://test") as ac:
        response = await ac.get("/")
    assert response.status_code == 200
    assert response.json()['timestamp'] == datetime.utcnow().strftime("%Y.%m.%d %H:%M")
    assert checkIpFormat(response.json()['ip']) == True


def checkIpFormat(ip):
    try:
        ipaddress.IPv4Address(str(ip))
        
    except ipaddress.AddressValueError:
        return False
    
    return True