from datetime import date, datetime
from ipaddress import ip_address
import ipaddress
import json
from time import strftime
from fastapi import FastAPI, Request
from datetime import datetime
from pydantic import BaseModel


tags_metadata = [
    {
        "name": "ip and date",
        "description": """Address ip host,
                        Datatime in format YYYY.MM.DD HH:MM""",
    }
]
description =""
f = open("README.md")
for line in f:
    description += line
app = FastAPI(
    title="IpDateApi",
    description=description,
    

    version="0.75.2",
    contact={
        "name": "Wojciech Maj",
        "url": "https://gitlab.com/Wojtas059/procject_task_1",
        "email": "wojtas.maj34@gmail.com",
    },
    openapi_tags=tags_metadata)

def parse_date(date_time: datetime):
        return date_time.strftime("%Y.%m.%d %H:%M")

class IpDatetime(BaseModel):
    ip: str
    timestamp: datetime
    
    class Config:
        json_encoders = {
            datetime: parse_date  
        }
    
    

# Function GET type, that return ip-host and date-time,
# date time returns time  from   Time Zone = UTC/GMT

@app.get("/",response_model=IpDatetime ,tags=["ip and date"])
async def get_ip_timestamp(request: Request):
    data = {'ip':request.client.host, 'timestamp': datetime.utcnow()}
    return IpDatetime(**data)


def get_app():
    return app
